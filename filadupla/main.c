#include <stdio.h>
#include <stdlib.h>

#include <fila.h>

typedef struct item {
  char *nome;
} Item;

Item* new(char *nome) {
  Item *item = (Item*) malloc(sizeof(Item));

  item->nome = nome;

  return item;
}

void item_print(void *_item) {
  Item *item = (Item*) _item;
  printf("%s ", item->nome);
}

int main(void) {

  Fila *fila = fila_init();

  fila_ins(fila, 0, new("M"));

  fila_ins(fila, 0, new("A"));
  fila_ins(fila, 0, new("B"));
  fila_ins(fila, 0, new("C"));

  fila_ins(fila, 1, new("Z"));
  fila_ins(fila, 1, new("Y"));

  fila_each(fila, 0, item_print);
  puts("");

  fila_each(fila, 1, item_print);
  puts("");

  return 0;
}
