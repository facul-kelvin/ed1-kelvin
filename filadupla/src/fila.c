
#include <stdlib.h>
#include <fila.h>

typedef struct node Node;

typedef struct sides {
  Node* p[2];
} Sides;

struct node {
  Sides sds;
  void* item;
};

struct fila {
  Sides sds;
};

Node * gs(void *obj, int side) {
  side = !!side;
  return ((Sides*)obj)->p[side];
}

Node * ss(void *obj, int side, Node *nd) {
  side = !!side;
  ((Sides*)obj)->p[side] = nd;
}

Node * node_init(void *item) {
  Node *nd = (Node*) malloc(sizeof(Node));
  ss(nd, 0, NULL);
  ss(nd, 1, NULL);
  nd->item = item;
  return nd;
}

Fila * fila_init() {
  Fila *fl = (Fila*) malloc(sizeof(Fila));
  ss(fl, 0, NULL);
  ss(fl, 1, NULL);
  return fl;
}

int fila_empty(Fila *fila, int side) {
  return gs(fila, side) == NULL;
}

Node * fila_ins(Fila *fila, int side, void *item) {
  Node *new = node_init(item);
  Node *old = gs(fila, side);

  ss(fila, side, new);
  if (!old) {
    ss(fila, !side, new);
  } else {
    ss(new, side, old);
    ss(old, !side, new);
  }
  
  return new;
}

void * fila_pop(Fila *fila, int side);

void fila_each(Fila *fila, int side, void(*fn)(void *item)) {
  Node *nd = gs(fila, side);
  while (nd) {

    fn( nd->item );

    nd = gs(nd, side);
  }
}
