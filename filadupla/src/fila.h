
typedef struct fila Fila;
typedef struct node Node;


Fila * fila_init();

int fila_empty(Fila *fila, int side);

Node * fila_ins(Fila *fila, int side, void *item);

void * fila_pop(Fila *fila, int side);

void fila_each(Fila *fila, int side, void(*fn)(void *item));

