 
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>

#define TAM_TAB 127

#define ALOCA_TIPO(TIPO) ( (TIPO*) malloc(sizeof(TIPO)) )
#define ALOCA_STR(LEN) ( (char*) malloc((LEN) + 1) )

typedef unsigned int uint;

typedef struct item Item;
struct item {
    char *palavra;
    int contagem;
    Item *prox;
};

typedef struct {
    Item *vet[TAM_TAB];
    uint total;
} Tabela;


char * strClone(char *str) {
    char *nova = ALOCA_STR(strlen(str));
    strcpy(nova, str);
    return nova;
}

int strEq(char *str1, char *str2) {
    return strcmp(str1, str2) == 0;
}

uint hashPalavra(char *str) {
    uint hash = 0;
    char c; // com sinal ?
    while ( c = *(str++), c != '\0' ) {
        hash += c;
    }
    return hash % TAM_TAB;
}

Tabela * Tabela_init() {
    Tabela *tab = ALOCA_TIPO(Tabela);
    tab->total = 0;
    for (uint i = 0; i < TAM_TAB; i++) {
        tab->vet[i] = NULL;
    }
    return tab;
}

Item * Item_cria(char *palavra) {
    Item *it = ALOCA_TIPO(Item);
    it->palavra = strClone(palavra);
    it->contagem = 0;
    it->prox = NULL;
    return it;
}

int Item_inc(Item *it) {
    return ++(it->contagem);
}


Item * Tabela_pegaItem(Tabela *tab, char *palavra) {
    uint hash = hashPalavra(palavra);
    Item *p = tab->vet[hash];
    if (p == NULL) {
        fprintf(stderr, "Posição vazia\n");
        tab->vet[hash] = Item_cria(palavra);
        tab->total++;
        return tab->vet[hash];
    }
    fprintf(stderr, "Colisão\n");
    Item *ant;
    while ( p && !strEq(palavra, p->palavra) ) {
        ant = p;
        p = p->prox;
    }
    if (p == NULL) {
        p = ant->prox = Item_cria(palavra);
        tab->total++;
    }
    return p;
}

int lePalavra(FILE *arq, char *str) {
    char c;
    while ( c = fgetc(arq), !feof(arq) && isspace(c) );
    if (feof(arq)) return 0;
    while ( !feof(arq) && !isspace(c) ) {
        *(str++) = c;
        c = fgetc(arq);
    }
    *str = '\0';
    return 1;
}

int main() {
    char palavra[1024];

    Tabela *tabela = Tabela_init();

    // printf("%d\n", scanf("%s", palavra));

    // while ( lePalavra(stdin, palavra) ) {
    while (scanf("%s", palavra) == 1) {
        printf("palavra: [%s]\n", palavra);
        Item *it = Tabela_pegaItem(tabela, palavra);
        Item_inc(it);
    }

    int c = 0;
    for (int i = 0; i < TAM_TAB; i++) {
        printf("%10p ", tabela->vet[i]);
        if (c++ == 8) {
            printf("\n");
            c = 0;
        }
    }
    printf("\n\n");

    c = 0;
    for (int i = 0; i < TAM_TAB; i++) {
        c = 0;
        Item *it = tabela->vet[i];
        while (it) {
            printf("c: %d\n", c++);
            printf("[%s] : %u\n", it->palavra, it->contagem);
            it = it->prox;
        }
    }

    return 0;
}
