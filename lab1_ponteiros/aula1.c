
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define EXECUTA(n) main_##n();

// declaração da main de cada exercício
int main_1();
int main_2();
int main_3();
int main_4();

int main () {
  return EXECUTA(4) // exercício a ser executado
}


/* Exercício 1 */

void calc_esfera (float r, float* area, float* volume) {
  *area = 4 * M_PI * pow(r, 2);
  *volume = 4 * M_PI * pow(r, 3) / 3;
}

int main_1 () {
  printf("== Exercicio 1 ==\n");

  float raio = 10;
  float area, volume;

  printf("raio: %.4f\n", raio);
  calc_esfera(10, &area, &volume);

  printf("area: %.4f, volume: %.4f\n", area, volume);

  // de acordo com o Wolfram Alpha, uma esfera de 10 de raio deve ter
  // raio area de 1256.64 e volume de 4188.79
  // https://www.wolframalpha.com/input/?i=sphere+10+of+radius

  return 0;
}


/* Exercício 2 */

int negativos (int n, float* vet) {
  int cont = 0;
  for (int i=0; i<n; i++)
    if (*(vet++) < 0) cont++;
  return cont;
}

int main_2 () {
  printf("== Exercicio 2 ==\n");

  int n = 9;
  float* valores = (float*) malloc(n * sizeof(float));

  valores[0] = 7;
  valores[1] = -2;
  valores[2] = 9;
  valores[3] = -5.2;
  valores[4] = 0.01;
  valores[5] = -1;
  valores[6] = 5;
  valores[7] = -1000;
  valores[8] = 0;

  int negs = negativos(9, valores);
  printf("quantidade de números negativos: %d\n", negs);

  free(valores);

  return 0;
}


/* Exercício 3 */

void imprimeInts (int n, int* vet) {
  for (int i = 0; i < n; i++)
    printf("%d ", vet[i]);
}

void swapInt (int *a, int* b) {
  int temp = *a;
  *a = *b;
  *b = temp;
}

void inverte (int n, int* vet) {
  int* last = vet+n-1;
  for (int i = 0; i < n/2; i++)
    swapInt(vet+i, last-i);
}

int main_3 () {
  printf("== Exercicio 3 ==\n");

  int n = 9;
  int* valores = (int*) malloc(n * sizeof(int));
  for (int i = 0; i < n; i++) valores[i] = i+1; // inicializa o vetor com numeros naturais

  printf("original:  ");
  imprimeInts(n, valores);
  printf("\n");

  inverte(n, valores);

  printf("invertido: ");
  imprimeInts(n, valores);
  printf("\n");

  free(valores);

  return 0;
}


/* Exercício 4 */

int printInts (int n, int* valores) {
  for (int i = 0; i < n; i++)
    printf("%d ", valores[i]);
}

int printIntsByPts (int n, int** ponts) {
  for (int i = 0; i < n; i++)
    printf("%d ", *(ponts[i]));
}

int compPontInt (const void *pa, const void *pb) {
  int* a = *((int**)pa);
  int* b = *((int**)pb);
  return *a - *b;
}

// inverte?
int** inverte2 (int n, int* vet) {
  int** ponts = (int**) malloc(n * sizeof(int*));
  for (int i = 0; i < n; i++)
    ponts[i] = vet+i;
  qsort(ponts, n, sizeof(int*), compPontInt);
  return ponts;
}

int main_4 () {
  printf("== Exercicio 4 ==\n");

  int n = 6;
  int valores[6] = {9, 3, 5, 1, 8, 4};

  printInts(n, valores); printf("\n");

  int** ordenado = inverte2(n, valores);

  printIntsByPts(n, ordenado);  printf("\n");

  free(ordenado);

  return 0;
}

//
