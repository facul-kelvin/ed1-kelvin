
#include <stdio.h>
#include <stdlib.h>
#include "matriz.h"

struct matriz {
    int m;
    int n;
    int** vet;
};

/*Inicializa uma matriz de nlinhas e ncolunas
* inputs: nlinhas (no de linhas) e ncolunas (no de colunas)
* output: ponteiro para a matriz inicializada
* pre-condicao: nlinhas e ncolunas diferentes de 0
* pos-condicao: matriz de retorno existe e contem nlinhas e ncolunas
*/
Matriz* inicializaMatriz (int nlinhas, int ncolunas) {
    Matriz* matriz = (Matriz*) malloc(sizeof(Matriz));
    matriz->m = nlinhas;
    matriz->n = ncolunas;
    matriz->vet = (int**) malloc(matriz->m * sizeof(int*));
    for (int i = 0; i < matriz->m; i++) {
        matriz->vet[i] = (int*) malloc(matriz->n * sizeof(int));
    }
    return matriz;
}

/*Modifica o elemento [linha][coluna] da matriz mat
* inputs: a matriz, a linha, a coluna, e o novo elemento
* output: nenhum
* pre-condicao: matriz mat existe, linha e coluna são válidos na matriz
* pos-condicao: elemento [linha][coluna] da matriz modificado
*/
void modificaElemento (Matriz* mat, int linha, int coluna, int elem) {
    mat->vet[linha][coluna] = elem;
}

/*Retorna o elemento mat[linha][coluna]
* inputs: a matriz, a linha e a coluna
* output: elemento mat[linha][coluna]
* pre-condicao: matriz mat existe, linha e coluna são válidos na matriz
* pos-condicao: mat não é modificada
*/
int recuperaElemento(Matriz* mat, int linha, int coluna) {
    return mat->vet[linha][coluna];
}

/*Retorna o número de colunas da matriz mat
* inputs: a matriz
* output: o número de colunas da matriz
* pre-condicao: matriz mat existe
* pos-condicao: mat não é modificada
*/
int recuperaNColunas (Matriz* mat) {
    return mat->n;
}

/*Retorna o número de linhas da matriz mat
* inputs: a matriz
* output: o número de linhas da matriz
* pre-condicao: matriz mat existe
* pos-condicao: mat não é modificada
*/
int recuperaNLinhas (Matriz* mat) {
    return mat->m;
}

/*Retorna a matriz transposta de mat
* inputs: a matriz
* output: a matriz transposta
* pre-condicao: matriz mat existe
* pos-condicao: mat não é modificada e matriz transposta existe
*/
Matriz* transposta (Matriz* mat) {
    Matriz* transp = inicializaMatriz(mat->n, mat->m);
    for (int i = 0; i < mat->m; i++)
        for (int j = 0; j < mat->n; j++)
            modificaElemento(transp, j, i, recuperaElemento(mat, i, j));
    return transp;
}

/*Retorna a matriz multiplicacao entre mat1 e mat2
* inputs: as matrizes mat1 e mat2
* output: a matriz multiplicação
* pre-condicao: matrizes mat1 e mat2 existem, e o numero de colunas de mat1
* correponde ao numero de linhas de mat2
* pos-condicao: mat1 e mat2 não são modificadas e a matriz multiplicacao existe
*/
Matriz* multiplicacao (Matriz* mat1, Matriz* mat2) {
    const int m = mat1->m;
    const int p = mat2->m;
    const int n = mat2->n;
    Matriz* res = inicializaMatriz(m, n);

    for (int i = 0; i < m; i++) {
        for (int j = 0; j < n; j++) {
            int e = 0;
            for (int k = 0; k < p; k++) {
                e += recuperaElemento(mat1, i, k) * recuperaElemento(mat2, k, j);
            }
            modificaElemento(res, i, j, e);
        }
    }
    return res;
}

/*Imprime a matriz
* inputs: matriz mat
* output: nenhum
* pre-condicao: matriz mat existe
* pos-condicao: nenhuma
*/
void imprimeMatriz(Matriz* mat) {
    for (int i = 0; i < mat->m; i++) {
        for (int j = 0; j < mat->n; j++) {
            printf("%4d ", recuperaElemento(mat, i, j));
        }
        printf("\n");
    }
}


/*Libera memória alocada para a matriz
* inputs: matriz mat
* output: nenhum
* pre-condicao: matriz mat existe
* pos-condicao: toda a memória alocada para matriz foi liberada
*/
void destroiMatriz(Matriz* mat) {
    for (int i = 0; i < mat->m; i++) {
        free(mat->vet[i]);
    }
    free(mat->vet);
    free(mat);
}
