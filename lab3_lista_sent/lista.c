
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "lista.h"

typedef struct celula Celula;

struct tipoitem {
  int matricula;
  char* nome;
  char* endereco;
};

struct celula {
  Celula *prox;
  TipoItem *item;
};

struct tipolista {
  Celula* prim;
  Celula* ult;
};

TipoItem* InicializaTipoItem(char* nome, int matricula, char* endereco) {
  TipoItem* item = (TipoItem*) malloc(sizeof(TipoItem));
  item->nome = (char*) malloc(sizeof(char) * (strlen(nome)+1));
  item->endereco = (char*) malloc(sizeof(char) * (strlen(endereco)+1));
  item->matricula = matricula;
  strcpy(item->nome, nome);
  strcpy(item->endereco, endereco);
  return item;
}

Celula* InicializaCelula (TipoItem* item) {
  Celula* celula = (Celula*) malloc(sizeof(Celula));
  celula->prox = NULL;
  celula->item = item;
  return celula;
}

TipoLista* InicializaLista() {
  TipoLista* lst = (TipoLista* )malloc(sizeof(TipoLista));
  lst->prim = NULL;
  lst->ult = NULL;
  return lst;
}

int ListaVazia (TipoLista* lista) {
  return (lista->prim == NULL);
}

void Insere (TipoItem* aluno, TipoLista* lista) {
  TipoItem* item = InicializaTipoItem(aluno->nome, aluno->matricula, aluno->endereco);
  Celula* cel = InicializaCelula(item);

  if (ListaVazia(lista)) {
    lista->prim = cel;
    lista->ult = cel;
  } else {
    cel->prox = lista->prim;
    lista->prim = cel;
  }
}

void Lista_forEachItem (TipoLista* lista, void (*func)(TipoItem* item)) {
  Celula* cel = lista->prim;
  while (cel != NULL) {
    func(cel->item);
    cel = cel->prox;
  }
}

void Lista_forEachCel (TipoLista* lista, void (*func)(Celula* item)) {
  Celula* cel = lista->prim;
  while (cel != NULL) {
    func(cel->item);
    cel = cel->prox;
  }
}

void Item_imprime (TipoItem* aluno) {
  printf("%-20s %-8d %s\n", aluno->nome, aluno->matricula, aluno->endereco);
}

void Imprime (TipoLista* lista) {
  Lista_forEachItem(lista, Item_imprime);
}

TipoItem* Retira (TipoLista* lista, int mat) {

}

TipoLista* Libera (TipoLista* lista) {

}


//
