
#include "fila.h"

#include <stdlib.h>
#include "pilha.h"

/*Tipo que define a fila (tipo opaco)
  Estrutura interna do tipo deve ser definida na implementa��o do TAD. Usar como base o TAD Pilha (pilha.h) - n�o usar outra estrutura de dados
  */
typedef struct fila {
    Pilha *pilha;
    Pilha *pilhaAux;
    int reversed;
} Fila;

/*Cria uma fila vazia, usando pilha
* inputs: nenhum
* output: fila alocada e vazia
* pre-condicao: nao tem
* pos-condicao: fila alocada e vazia*/
Fila* cria_fila(void) {
    Fila *fl = (Fila*) malloc(sizeof(Fila));
    fl->pilha = cria_pilha();
    fl->pilhaAux = cria_pilha();
    fl->reversed = 0;
    return fl;
}

/** Troca os elementos da pilha principal para auxiliar, e vice-versa */
void _reverse(Fila* fila) {
    Pilha *src = !fila->reversed ? fila->pilha : fila->pilhaAux;
    Pilha *target = fila->reversed ? fila->pilha : fila->pilhaAux;
    const int len = tamanho_pilha(src);
    for (int i = 0; i < len; i++) {
        push(pop(src), target);
    }
    fila->reversed = !fila->reversed;
}

/*Insere uma pessoa na fila de Pessoas (lembre-se, fila e' FIFO). Imprime mensagem de erro caso a fila esteja cheia.
* inputs: pessoa a ser inserida na fila (do tipo pessoa) e a fila
* output: nenhum 
* pre-condicao: pessoa e fila n�o s�o nulos
* pos-condicao: fila cont�m a pessoa inserida na ultima posi��o*/
void insere (Pessoa* pessoa, Fila* f) {
    if (f->reversed) _reverse(f);
    push(pessoa, f->pilha);
}

/*Retira uma pessoa da fila (usando FIFO). Imprime mensagem de erro caso a fila esteja vazia.
* inputs: a fila
* output: a Pessoa (do tipo Pessoa) retirada da Fila (ou NULL, caso a fila esteja vazia)
* pre-condicao: fila n�o � nula
* pos-condicao: fila n�o cont�m a pessoa que estava na primeira posicao */
Pessoa* retira (Fila* f) {
    if (!f->reversed) _reverse(f);
    return pop(f->pilhaAux);
}


/*Imprime os dados de todos as pessoas da fila (do inicio ao fim da fila!). Imprime mensagem de fila vazia, caso a fila esteja vazia.
* inputs: a fila de pessoas
* output: nenhum
* pre-condicao: fila n�o � nula
* pos-condicao: dados dos pessoas impressos na saida padrao
*/
void imprime_fila (Fila* f) {
    if (f->reversed) _reverse(f);
    imprime_pilha(f->pilha);
}


/*Verifica se a fila esta ou nao vazia
* inputs: a fila de pessoas
* output: 1 se estiver vazia e zero caso contrario
* pre-condicao: fila n�o � nula
* pos-condicao: nenhuma
*/
int Vazia_fila (Fila* f) {
    Pilha *atual = !f->reversed ? f->pilha : f->pilhaAux;
    return vazia_pilha(atual);
}


/*Separa a fila f de tal forma que as pessoas com idade maior que 60 sejam movidas para a fila "f_maiores" e as pessoas com idade menor que 60 para a fila "f_menores".
* inputs: a fila de pessoas, as filas f_maiores e f_menores (inicialmente vazias)
* output: nao tem
* pre-condicao: fila f n�o � nula
* pos-condicao: fila f vazia, pessoas com idade maior ou igual a 60 na fila "f_maiores" e as pessoas com idade menor que 60 para a fila "f_menores" */
void separa_filas (Fila* f, Fila* f_maiores, Fila* f_menores);


/*Libera toda a mem�ria utilizada pelas estruturas de dados.
* inputs: a fila de pessoas
* output: NULL
* pre-condicao: fila f n�o � nula
* pos-condicao: Mem�ria liberada */
Fila* destroi_fila (Fila* f) {
    destroi_pilha(f->pilha);
    destroi_pilha(f->pilhaAux);
    free(f);
    return NULL;
}
