
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define ALOCA_TIPO(TIPO) ((TIPO*) malloc(sizeof(TIPO)))

char * strClone (char *str) {
  char *newStr = (char*) malloc(strlen(str) + 1);
  strcpy(newStr, str);
  return newStr;
}

typedef enum {
  //CLIENTE,
  MOVEL,
  IMOVEL,
} TipoItem;

/*Estrutura Cliente(tipo opaco)
  Estrutura interna do tipo deve ser definida na implementa��o do TAD. Devem ser definidos campos:
  - nome (string)
  - id (int)
  */
typedef struct cliente {
  // TipoItem tipo;
  char *nome;
  int id;
} Cliente;

/*Estrutura Movel(tipo opaco)
  Estrutura interna do tipo deve ser definida na implementa��o do TAD. Devem ser definidos campos:
  - placa (int)
  - ano (int)
  - valor (float)
  */
typedef struct movel {
  TipoItem tipo;
  int placa;
  int ano;
  float valor;
} Movel;

/*Estrutura Imovel(tipo opaco)
  Estrutura interna do tipo deve ser definida na implementa��o do TAD. Devem ser definidos campos:
  - identificador (int)
  - ano (int)
  - valor (float)
  */
typedef struct imovel {
  TipoItem tipo;
  int id;
  int ano;
  float valor;
} Imovel;


/*Tipo que define a lista heterogenea(tipo opaco)
  Estrutura interna do tipo deve ser definida na implementa��o do TAD.  Use o ponteiro gen�rico para implementar a lista (void*)
  Campos da celula da lista: 
   - dono (Cliente*)
   - item (void*)
   - Prox (struct listahet*)
   - identificador do item (int)
*/
typedef struct listahet ListaHet;
struct listahet {
  Cliente *dono;
  void *item;
  ListaHet *prox;
};

/*Cria lista vazia
* inputs: nenhum
* output: lista vazia
* pre-condicao: nenhuma
* pos-condicao: lista criada e vazia
*/
ListaHet * cria () {
  return NULL;
}

/*Cria uma estrutura do tipo Cliente
* inputs: nome do cliente e o identificador do cliente
* output: Endere�o da estrutura cliente criada
* pre-condicao: nome e id validos
* pos-condicao: estrutura cliente criada, com os campos nome e id corretamente atribuidos
*/
Cliente * cria_cliente (char* nome, int id) {
  Cliente *cliente = ALOCA_TIPO(Cliente);
  *cliente = (Cliente){ 
    strClone(nome),
    id,
   };
   return cliente;
}


/*Cria uma estrutura do tipo Movel
* inputs: placa, ano de fabricacao e valor do automovel 
* output: Endere�o da estrutura movel criada
* pre-condicao: placa, ano e valor validos
* pos-condicao: estrutura movel criada
*/
Movel * cria_movel (int placa, int ano, float valor) {
  Movel *movel = ALOCA_TIPO(Movel);
  *movel = (Movel){
    MOVEL,
    placa,
    ano,
    valor,
   };
   return movel;
}

/*Cria uma estrutura do tipo Imovel
* inputs: identificador, ano de construcao e o valor do imovel
* output: Endere�o da estrutura imovel criada
* pre-condicao: id, ano e valor validos
* pos-condicao: estrutura imovel criada
*/
Imovel * cria_imovel (int id, int ano, float valor) {
  Imovel *imovel = ALOCA_TIPO(Imovel);
  *imovel = (Imovel){
    IMOVEL,
    id,
    ano,
    valor,
   };
   return imovel;
}

/*Insere um item (do tipo Movel) na primeira posicao da lista 
* inputs: a lista, o cliente dono do automovel e o item a ser inserido
* output: a lista atualizada
* pre-condicao: lista, cliente e automovel validos
* pos-condicao: lista atualizada com o item inserido na primeira posicao
*/
ListaHet* insere_movel (ListaHet* lista, Cliente* dono, Movel* item) {
  ListaHet *celula = ALOCA_TIPO(ListaHet);
  celula->prox = lista;
  celula->dono = dono;
  celula->item = item;
  return celula;
}


/*Insere um item (do tipo Imovel) na primeira posicao da lista 
* inputs: a lista, o cliente dono do imovel e o item a ser inserido
* output: a lista atualizada
* pre-condicao: lista, cliente e imovel validos
* pos-condicao: lista atualizada com o item inserido na primeira posicao
*/
ListaHet* insere_imovel (ListaHet* lista, Cliente* dono, Imovel* item) {
  ListaHet *celula = ALOCA_TIPO(ListaHet);
  celula->prox = lista;
  celula->dono = dono;
  celula->item = item;
  return celula;
}

/*Imprime os elementos da lista. Para cada elemento da lista, deve-se imprimir os dados do Cliente, seguido dos dados do im�vel (caso o item seja um im�vel) ou dos dados do autom�vel (caso o item seja um movel)
* inputs: a lista
* output: nao tem
* pre-condicao: lista valida
* pos-condicao: lista inalterada
*/
void imprime (ListaHet* lista) {
  ListaHet *cel = lista;
  while (cel) {
    Cliente *cli = cel->dono;
    printf("Cliente %d (%s)", cli->id, cli->nome);
    printf(" : ");
    TipoItem tipo = *((TipoItem*) cel->item);
    if (tipo == MOVEL) {
      Movel *item = (Movel*) cel->item;
      printf("Movel: %d %d %f\n", item->placa, item->ano, item->valor);
    } else if (tipo == IMOVEL) {
      Imovel *item = (Imovel*) cel->item;
      printf("Imovel: %d %d %f\n", item->id, item->ano, item->valor);
    } else {
      printf("TIPO DESCONHECIDO %d\n", tipo);
    }
    cel = cel->prox;
  }
}

/*Retira da lista todos os itens assegurados de um dado cliente 
* inputs: a lista e o identificador do cliente
* output: lista atualizada
* pre-condicao: lista e identificador do cliente validos
* pos-condicao: lista nao contem itens do cliente cujo identificador eh id_cliente
*/
ListaHet* retira_cliente (ListaHet* lista, int id_cliente) {
  ListaHet *ret = lista;
  ListaHet *ant = NULL;
  ListaHet *cel = lista;
  while (cel) {
    Cliente *dono = cel->dono;

    if (dono->id == id_cliente){

      TipoItem tipo = *((TipoItem*) cel->item);
      if (tipo == MOVEL) {
        Movel *item = (Movel*) cel->item;
        free(item); // liberar campos
      } else if (tipo == IMOVEL) {
        Imovel *item = (Imovel*) cel->item;
        free(item); // liberar campos
      } else {
        printf("TIPO DESCONHECIDO %d\n", tipo);
      }
      ListaHet *prox = cel->prox;
      if (ant)
        ant->prox = prox;
      else
        ret = prox;
      free(cel); // liberar campos
      cel = prox;
      continue;
    }

    ant = cel;
    cel = cel->prox;
  }
  return ret;
}

/*Calcula o valor total assegurado de um dado cliente (incluindo moveis e imoveis). Note que o valor assegurado depende da taxa estipulada para o tipo do item. O calculo do valor assegurado para UM dado item deve ser: valor_item*taxa_item
* inputs: a lista, o endereco do cliente, e as taxas para o calculo do valor assegurado. 
* output: valor total assegurado, que deve ser a soma dos valores assegurados de todos os itens do dado cliente
* pre-condicao: lista e identificador do cliente validos
* pos-condicao: lista inalterada
*/
float calcula_valor_assegurado (ListaHet* lista, Cliente* dono, float taxa_movel, float taxa_imovel) {
  return 0; // acabou o tempo
};
