
#include <stdlib.h>
#include <stdio.h>

typedef struct arv Arv;

struct arv {
    Arv *e;
    Arv *d;
    char c;
};

// Cria uma árvore vazia
Arv* arv_criavazia (void) {
    return NULL;
}

// Cria uma árvore com a informação do nó raiz c, e
// Com subárvore esquerda e e subárvore direita d
Arv* arv_cria (char c, Arv* e, Arv* d) {
    Arv *arv = (Arv*)malloc(sizeof(Arv));
    arv->e = e;
    arv->d = d;
    arv->c = c;
    return arv;
}

// Libera o espaço de memória ocupado pela árvore a
Arv* arv_libera (Arv* a) {
    if (!a) return NULL;
    arv_libera(a->e);
    arv_libera(a->d);
    free(a);
    return NULL;
}

//retorna true se a �rvore estiver vazia e false 
//caso contr�rio
int arv_vazia (Arv* a) {
    return !a;
}

//indica a ocorr�ncia (1) ou n�o (0) do caracter c
int arv_pertence (Arv* a, char c) {
    return 
        (a->c == c)
        || (a->e && arv_pertence(a->e, c))
        || (a->d && arv_pertence(a->d, c));
}

void _arv_imprime(Arv *a, int nv) {
    for (int i=0; i<nv; i++) printf("  ");
    if (a) {
        printf("<%c", a->c);
        if (a->e || a->d) {
            printf("\n");
            _arv_imprime(a->e, nv+1);
            printf("\n");
            _arv_imprime(a->d, nv+1);
            printf("\n");
            for (int i=0; i<nv; i++) printf("  ");
        }
        printf(">");
    } else {
        printf("<>");
    }
}

// imprime as informações dos n�s da �rvore
void arv_imprime (Arv* a) {
    _arv_imprime(a, 0);
    printf("\n");
}

Arv *_arv_pai(Arv* a, char c, Arv* p) {
    if (!a) return NULL;
    if (a->c == c)
        return p;
    Arv *res = _arv_pai(a->e, c, a);
    if (res) return res;
    return _arv_pai(a->d, c, a);
}

// retorna o pai de um dado no
Arv* arv_pai (Arv* a, char c) {
    return _arv_pai(a, c, NULL);
}

// retorna a quantidade de folhas de uma arvore binaria
int folhas (Arv* a) {
    if (!a) return 0;
    if (!a->e && !a->d)
        return 1;
    return 
    (a->e ? folhas(a->e) : 0) + (a->d ? folhas(a->d) : 0);
}

// retorna o numero de ocorrencias de um dado caracter na arvore 
int ocorrencias (Arv* a, char c) {
    if (!a) return 0;
    return (a->c == c)
        + ocorrencias(a->e, c)
        + ocorrencias(a->d, c);
}

// retorna o campo informacao de um dado no
char info (Arv* a) {
    if (!a) return '\0';
    return a->c;
}
