
typedef struct arv Arv;

// Cria uma árvore vazia
Arv* arv_criavazia (void);

// Cria uma árvore com a informação do nó raiz c, e
// Com subárvore esquerda e e subárvore direita d
Arv* arv_cria (char c, Arv* e, Arv* d);

// Libera o espaço de memória ocupado pela árvore a
Arv* arv_libera (Arv* a);

//retorna true se a �rvore estiver vazia e false 
//caso contr�rio
int arv_vazia (Arv* a);

//indica a ocorr�ncia (1) ou n�o (0) do caracter c
int arv_pertence (Arv* a, char c);

//imprime as informa��es dos n�s da �rvore
void arv_imprime (Arv* a);

//retorna o pai de um dado no
Arv* arv_pai (Arv* a, char c);

//retorna a quantidade de folhas de uma arvore binaria
int folhas (Arv* a);

//retorna o numero de ocorrencias de um dado caracter na arvore 
int ocorrencias (Arv* a, char c);

//retorna o campo informacao de um dado no
char info (Arv* a);
