
#include <stdlib.h>

#define ALOCA_TIPO(TIPO) ((TIPO*) malloc(sizeof(TIPO)))

typedef struct celula Cel;

struct celula {
    Cel *prox;
    void *data;
};

typedef struct lista {
    Cel *prim;
    Cel *ult;
} Lista;

Lista * Lista_init() {
    Lista *lista = ALOCA_TIPO(Lista);
    *lista = (Lista){NULL, NULL};
    lista->prim = NULL;
    lista->ult = NULL;
    return lista;
}

Lista * Lista_libera(Lista *lista, void(*cb)(void *item)) {
    Cel *cl = lista->prim;
    while (cl) {
        Cel *prox = cl->prox;
        cb(cl->data);
        free(cl);
        cl = prox;
    }
    free(lista);
    return NULL;
}


void Lista_adiciona(Lista *lista, void *item) {
    if (!lista)
        return;

    Cel *cel = ALOCA_TIPO(Cel);
    cel->data = item;
    cel->prox = NULL;
    
    if (!lista->prim) {
        lista->prim = cel;
    } else {
        lista->ult->prox = cel;
    }
    lista->ult = cel;
}

int Lista_percorre(Lista *lista, int cb(void*, void*), void *param) {
    if (!lista)
        return 0;

    Cel *cl = lista->prim;
    while (cl) {

        int ret = cb(cl->data, param);
        if (ret) return ret;

        cl = cl->prox;
    }

    return 0;
}
