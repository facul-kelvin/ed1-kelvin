#ifndef _H_LISTAGEN
#define  _H_LISTAGEN


typedef struct lista Lista;


Lista * Lista_init();

Lista * Lista_libera(Lista *lista, void(*cb)(void *item));


void Lista_adiciona(Lista *lista, void *item);

int Lista_percorre(Lista *lista, int cb(void*, void*), void *param);


#endif //_H_LISTAGEN