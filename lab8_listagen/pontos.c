#include "pontos.h"

#include <stdlib.h>
#include <stdio.h>
#include "listagen.h"

#define ALOCA_TIPO(TIPO) ((TIPO*) malloc(sizeof(TIPO)))


struct ponto {
    float x;
    float y;   
};

Ponto * Ponto_cria(float x, float y) {
    Ponto *ponto = ALOCA_TIPO(Ponto);
    *ponto = (Ponto){x, y};
    return ponto;
}

void Ponto_libera(void *ponto) {
    free(ponto);
}


int Ponto_imprime(void *_ponto, void *param) {
    Ponto *ponto = _ponto;
    printf("(%f, %f)", ponto->x, ponto->y);
    return 0;
}

int Ponto_imprimeLn(void *_ponto, void *param) {
    Ponto *ponto = _ponto;
    printf("(%f, %f)\n", ponto->x, ponto->y);
    return 0;
}

int Ponto_igual(void *_p1, void *_p2) {
    Ponto *ponto1 = _p1;
    Ponto *ponto2 = _p2;
    return (ponto1->x == ponto2->x) && (ponto1->y == ponto2->y);
}

typedef struct { Ponto *centro; int quant; } calcCentro_t;

int _calcCentroGem(void *_ponto, void *_param) {
    Ponto *ponto = _ponto;
    calcCentro_t *param = _param;

    Ponto *centro = param->centro;

    centro->x += ponto->x;
    centro->y += ponto->y;

    param->quant++;

    return 0;
}

Ponto * Ponto_calcCentroGem(Lista *lista) {
    Ponto *centro = Ponto_cria(0, 0);
    calcCentro_t param = {centro, 0};
    
    Lista_percorre(lista, _calcCentroGem, &param);
    centro->x /= param.quant;
    centro->y /= param.quant;

    return centro;
}