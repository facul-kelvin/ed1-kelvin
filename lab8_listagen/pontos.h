#ifndef _H_PONTOS
#define _H_PONTOS

#include "listagen.h"

typedef struct ponto Ponto;


Ponto * Ponto_cria(float x, float y);

void Ponto_libera(void *ponto);


int Ponto_imprime(void *_ponto, void *param);

int Ponto_imprimeLn(void *_ponto, void *param);

int Ponto_igual(void *_p1, void *_p2);

Ponto * Ponto_calcCentroGem(Lista *lista);


#endif //_H_PONTOS