
#include <stdlib.h>
#include <stdio.h>

#include "listagen.h"
#include "pontos.h"

int main() {

    Lista* lista = Lista_init();

    Lista_adiciona(lista, Ponto_cria(0.0, 0.0));
    Lista_adiciona(lista, Ponto_cria(0.0, 4.0));
    Lista_adiciona(lista, Ponto_cria(4.0, 4.0));
    Lista_adiciona(lista, Ponto_cria(4.0, 0.0));

    Lista_percorre(lista, Ponto_imprimeLn, NULL);

    Ponto * PONTO; int pert; char *nao;

    PONTO = Ponto_cria(4.0, 4.0);
    pert = Lista_percorre(lista, Ponto_igual, PONTO);
    nao = pert ? "" : " não";
    printf("O ponto "); Ponto_imprime(PONTO, NULL); printf("%s existe\n", nao);
    Ponto_libera(PONTO);

    PONTO = Ponto_cria(3.0, 7.0);
    pert = Lista_percorre(lista, Ponto_igual, PONTO);
    nao = pert ? " " : " não";
    printf("O ponto "); Ponto_imprime(PONTO, NULL); printf("%s existe\n", nao);
    Ponto_libera(PONTO);


    Ponto *centro = Ponto_calcCentroGem(lista);
    printf("Centro: "); Ponto_imprime(centro, NULL); printf("\n");
    Ponto_libera(centro);

    Lista_libera(lista, Ponto_libera);

    return 0;
}