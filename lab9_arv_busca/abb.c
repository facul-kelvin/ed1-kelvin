#include "abb.h"

#include <stdlib.h>
#include <stdio.h>

#define ALOCA_TIPO(TIPO) ( (TIPO*) malloc(sizeof(TIPO)) )

struct abb {
    int dado;
    Abb *esq;
    Abb *dir;
};

Abb * abbCriaVazia() {
    return NULL;
}

Abb * abbLibera(Abb *arv) {
    if (!arv)
        return NULL;
    abbLibera(arv->esq);
    abbLibera(arv->dir);
    free(arv);
    return NULL;
}

Abb * abbInsere(Abb *arv, int dado) {
    if (!arv) {
        Abb *folha = ALOCA_TIPO(Abb);
        folha->dado = dado;
        folha->esq = NULL;
        folha->dir = NULL;
        return folha;
    }

    // if (dado < arv->dado)
    //     arv->esq = abbInsere(arv->esq, dado);
    // else 
    //     arv->dir = abbInsere(arv->dir, dado);

    Abb** lado = 
        (dado < arv->dado) ? &arv->esq : &arv->dir;
    
    *lado = abbInsere(*lado, dado);

    return arv;
}


void _imprime(Abb *arv) {
    if (!arv) {
        // printf(".");
        return;
    }
    printf("(%d", arv->dado);
    _imprime(arv->esq);
    _imprime(arv->dir);
    printf(")");
}

void abbImprime(Abb *arv) {
    _imprime(arv);
    printf("\n");
}

void _imprimeOrd(Abb *arv) {
    if (!arv)
        return;
    _imprimeOrd(arv->esq);
    printf("%d ", arv->dado);
    _imprimeOrd(arv->dir);
}

void abbImprimeOrd(Abb *arv) {
    _imprimeOrd(arv);
    printf("\n");
}


Abb * abbBusca(Abb *arv, int dado) {
    if (!arv)
        return NULL;
    if (arv->dado == dado)
        return arv;
    
    // if (dado < arv->dado)
    //     return abbBusca(arv->esq, dado);
    // else
    //     return abbBusca(arv->dir, dado);

    Abb* lado = 
        (dado < arv->dado) ? arv->esq : arv->dir;

    return abbBusca(lado, dado);
}

Abb * abbRetira(Abb * arv, int dado) {
    if (!arv)
        return NULL;
    
    if (dado < arv->dado) {
        arv->esq = abbRetira(arv->esq, dado);
        return arv;
    } 
    if (arv->dado < dado) {
        arv->dir = abbRetira(arv->dir, dado);
        return arv;
    }

    // Se é o nó a ser retirado

    // Nenhum filho
    if (!arv->esq && !arv->dir) {
        free(arv);
        return NULL;
    }

    // Se tem filho direito
    if (!arv->esq) {
        Abb *rt = arv;
        arv = arv->dir;
        free(rt);
        return arv;
    }

    // Se tem filho esquerdo
    if (!arv->dir) {
        Abb *rt = arv;
        arv = arv->esq;
        free(rt);
        return arv;
    }

    // Se tem ambos filhos

    Abb *novo = arv->esq;
    while (novo->dir)
        novo = novo->dir;
    
    arv->dado = novo->dado;
    abbRetira(arv->esq, novo->dado);
    return arv;
}
