#ifndef _H_ABB 
#define _H_ABB

typedef struct abb Abb;


Abb * abbCriaVazia();

Abb * abbLibera(Abb *arv);

Abb * abbInsere(Abb *arv, int dado);


void abbImprime(Abb *arv);

void abbImprimeOrd(Abb *arv);


Abb * abbBusca(Abb *arv, int dado);

Abb * abbRetira(Abb * arv, int dado);


#endif // _H_ABB 