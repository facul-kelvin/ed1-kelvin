
#include <stdio.h>
#include "abb.h"

int main() {
    Abb *arv = abbCriaVazia();

    // arv = abbInsere(arv, 7);
    // arv = abbInsere(arv, 1);
    // arv = abbInsere(arv, 3);
    // arv = abbInsere(arv, 9);
    // arv = abbInsere(arv, 5);
    // arv = abbInsere(arv, 2);
    // arv = abbInsere(arv, 11);
    // arv = abbInsere(arv, 0);
    // arv = abbInsere(arv, 15);
    // arv = abbInsere(arv, 4);

    arv = abbInsere(arv, 6);
    arv = abbInsere(arv, 8);
    arv = abbInsere(arv, 2);
    arv = abbInsere(arv, 1);
    arv = abbInsere(arv, 4);
    arv = abbInsere(arv, 3);

    abbImprime(arv);
    abbImprimeOrd(arv);

    abbRetira(arv, 6);
    // abbRetira(arv, 2);
    abbImprime(arv);

    printf("Árvore do 3: ");
    abbImprime(abbBusca(arv, 3));

    printf("Árvore do 77: ");
    abbImprime(abbBusca(arv, 77));

    abbLibera(arv);

    return 0;
}
